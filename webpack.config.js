const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin({
    filename: "src/styles/[name].css",
});
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');

var FileLoader = require('file-loader');
module.exports = {
    context: path.join(__dirname, 'webpackSolution/'),
    entry: [
        "./src/main.app.module.js"
    ],
    output: {
        path: path.join(__dirname, '/build'),
        filename: "src/[name].js",
    },

    module: {
        rules: [{
            test: /\.scss$/,
            use: extractSass.extract({
                use: [{
                    loader: "css-loader"
                }, {
                    loader: "sass-loader"
                }],
                // use style-loader in development
                fallback: "style-loader"
            })
        }, {
            test: /\.js$/,
            loader: 'babel-loader'
        },
        {
            test: /\.html$/,
            loader: "file-loader?name=[path][name].[ext]"
        }],
        noParse: /angular\/angular.js/
    },
    plugins: [
        extractSass,
        new webpack.ProvidePlugin({
            '_': 'underscore',
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
        }),
        new CopyWebpackPlugin([
            { from: 'src/**/*.html', }
        ])
    ]
}